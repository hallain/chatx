import Koa from 'koa'
import logger from 'koa-logger'
import koaBody from 'koa-body'
import cors from 'koa-cors'
import { messagesRouter } from './api/message'
import { exceptionsMiddleware } from './middlewares/exceptions.middleware'

const app = new Koa()

app.use(logger())
app.use(koaBody())
app.use(cors())

app.use(exceptionsMiddleware())

app.use(messagesRouter.routes())
app.use(messagesRouter.allowedMethods())

app.listen(process.env.PORT, () => {
  console.log(`Koa started on port ${process.env.PORT}`)
})
