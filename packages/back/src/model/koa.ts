import Koa from 'koa'

export type KoaContext = Koa.ParameterizedContext
export type KoaNext = Koa.Next
