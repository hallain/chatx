import { KoaContext, KoaNext } from '../model/koa'
import { InputValidationError, DomainError } from '../model/exceptions'

export function exceptionsMiddleware() {
  return async function exceptionsMiddleware(ctx: KoaContext, next: KoaNext) {
    try {
      await next()
    } catch (error) {
      if (error instanceof InputValidationError) {
        ctx.status = 400
        ctx.body = { message: error.message }
      } else if (error instanceof DomainError) {
        ctx.status = 403
        ctx.body = { message: error.message }
      } else {
        ctx.status = 500
        ctx.body = { message: `Internal error` }
      }
    }
  }
}
