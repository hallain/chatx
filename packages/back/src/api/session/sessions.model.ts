import { KoaContext } from '../../model/koa'

export interface ISession {
  sessionId: string
  context: KoaContext
}
