import { KoaContext } from '../../model/koa'
import { sessionsRepository } from './session.repository'

// https://developer.mozilla.org/fr/docs/Web/API/Server-sent_events/Using_server-sent_events#envoyer_un_%C3%A9v%C3%A8nement_depuis_le_serveur

const create = (context: KoaContext) => {
  const session = sessionsRepository.add(context)

  const sessionCloseEvents = ['close', 'finish', 'error']
  sessionCloseEvents.forEach((eventName) => {
    context.req.on(eventName, (e) => {
      destroy(session.sessionId)
    })
  })

  console.log('\x1b[32m%s\x1b[0m', `Connected (${session.sessionId})`)

  return session
}

const destroy = (sessionId: string) => {
  const session = sessionsRepository.getById(sessionId)

  if (session) {
    session.context.res.end()
    sessionsRepository.remove(session.sessionId)

    console.log('\x1b[31m%s\x1b[0m', `Connection closed (${session?.sessionId})`)
  } else {
    // Already destroyed
    return
  }
}

const pushEvent = (context: KoaContext, eventName: string, message: any) => {
  const event = [
    `event: ${eventName}`,
    `data: ${JSON.stringify(message)}`,
    `retry: ${5 * 1000}`,
    '\n',
  ].join('\n')

  context.res.write(event)
}

const multicast = <T>(options: {
  eventName: string
  data: T
  filter: (context: KoaContext) => boolean
}) => {
  const { eventName, data, filter } = options
  sessionsRepository.getAll().forEach((session) => {
    if (filter(session.context)) {
      pushEvent(session.context, eventName, data)
    }
  })
}

const unicast = <T>(options: { eventName: string; data: T; sessionId: string }) => {
  const { data, eventName, sessionId } = options
  const session = sessionsRepository.getById(sessionId)
  if (session) {
    pushEvent(session?.context, eventName, data)
  }
}

export const sessionDomain = {
  create,
  destroy,
  multicast,
  unicast,
}
