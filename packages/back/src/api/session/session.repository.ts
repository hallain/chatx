import { v4 as uuidv4 } from 'uuid'
import { KoaContext } from '../../model/koa'
import { ISession } from './sessions.model'

let list: ISession[] = []

const getById = (sessionId: string) => {
  return list.find((session) => session.sessionId === sessionId)
}

const getAll = () => {
  return list
}

const add = (context: KoaContext) => {
  const session = {
    sessionId: uuidv4(),
    context,
  }
  list.push(session)
  return session
}

const remove = (id: string) => {
  list = list.filter((session) => session.sessionId !== id)
}

export const sessionsRepository = {
  getAll,
  getById,
  add,
  remove,
}
