import { v4 as uuidv4 } from 'uuid'
import { IMessage, IMessageCreation } from './message.model'

let list: IMessage[] = []

const getByRoomName = (roomName: string) => {
  return list.filter((message) => message.room === roomName)
}

const add = (message: IMessageCreation) => {
  const persistedMessage = { ...message, timestamp: new Date(), id: uuidv4() }
  list.push(persistedMessage)
  return persistedMessage
}

export const messageRepository = {
  getByRoomName,
  add,
}
