import { InputValidationError } from '../../model/exceptions'
import { IMessageCreation } from './message.model'
import { messageRepository } from './message.repository'

const listByRoom = (roomName: string) => messageRepository.getByRoomName(roomName)

const postMessage = (message: IMessageCreation) => {
  if (message.text === '') {
    throw new InputValidationError('Message can not be empty.')
  }

  return messageRepository.add(message)
}

export const messageDomain = {
  listByRoom,
  postMessage,
}
