import Router from 'koa-router'
import { PassThrough } from 'stream'
import { sessionDomain } from '../session/sessions.domain'
import { messageDomain } from './messages.domain'
import { IMessage, MESSAGE_EVENT } from './message.model'

const STREAM_TIMEOUT = 60 * 60 * 1000 // 1h en ms

export const messagesRouter = new Router()

messagesRouter.get('/sse/:roomname', async (ctx) => {
  const { roomname } = ctx.params
  const { sessionId } = sessionDomain.create(ctx)

  const stream = new PassThrough() // passes the input bytes across to the output

  ctx.set('Content-Type', 'text/event-stream')
  ctx.set('Connection', 'keep-alive')
  ctx.set('Cache-Control', 'no-cache')
  ctx.body = stream
  ctx.req.socket.setTimeout(STREAM_TIMEOUT)
  ctx.res.write('\n')

  sessionDomain.unicast({
    sessionId,
    eventName: MESSAGE_EVENT,
    data: messageDomain.listByRoom(roomname).slice(-10),
  })
})

messagesRouter.post('/message', async (ctx, next) => {
  const { text, nickname, room } = ctx.request.body

  const message = messageDomain.postMessage({
    nickname,
    text,
    room,
  })

  sessionDomain.multicast<IMessage[]>({
    eventName: MESSAGE_EVENT,
    filter: (context) => context.params.roomname == message.room,
    data: [message],
  })

  ctx.status = 201
  ctx.body = message

  await next()
})
