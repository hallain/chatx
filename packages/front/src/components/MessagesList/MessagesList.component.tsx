import { FC, useEffect, useRef } from 'react'
import { IMessage } from '../../model/message'
import { ChatMessage } from '../ChatMessage'
import styles from './MessagesList.module.scss'

interface IMessagesListProps {
  messages: IMessage[]
}

export const MessagesList: FC<IMessagesListProps> = ({ messages }) => {
  const bottomRef = useRef<HTMLDivElement>(null)

  useEffect(() => {
    if (bottomRef.current) {
      bottomRef.current.scrollIntoView({
        block: 'center',
        inline: 'start',
        behavior: 'smooth',
      })
    }
  }, [messages])

  return (
    <div className={styles.message}>
      {messages.map((message) => (
        <ChatMessage key={message.id} message={message} />
      ))}
      <div ref={bottomRef}></div>
    </div>
  )
}
