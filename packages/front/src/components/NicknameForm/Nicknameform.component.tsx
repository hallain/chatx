import { FC, useState } from 'react'
import styles from './NicknameForm.module.scss'

interface INicknameProps {
  onClose: (nickname: string) => void
}

export const NicknameForm: FC<INicknameProps> = ({ onClose }) => {
  const [value, setValue] = useState('')

  return (
    <div className={styles.container}>
      <div className={styles.form}>
        Nickname :
        <input
          className={styles.input}
          value={value}
          onChange={(e) => setValue(e.target.value)}
        ></input>
        <button className={styles.button} onClick={() => onClose(value)}>
          Choose
        </button>
      </div>
    </div>
  )
}
