import { FC } from 'react'
import { IMessage } from '../../model/message'
import { distanceDate, humanReadableDate } from '../../utils/datetime'
import styles from './ChatMessage.module.scss'

interface IChatMessageProps {
  message: IMessage
}

export const ChatMessage: FC<IChatMessageProps> = ({ message }) => {
  return (
    <div className={styles.message}>
      <div className={styles.avatar}>☹</div>
      <div>
        <div>
          <span className={styles.nickname}>{message.nickname}</span>
          <span className={styles.timestamp} title={humanReadableDate(message.timestamp)}>
            {distanceDate(message.timestamp)}
          </span>
        </div>
        <div className={styles.text}>{message.text}</div>
      </div>
    </div>
  )
}
