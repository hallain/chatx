import { FC } from 'react'
import styles from './RoomSelector.module.scss'
import classNames from 'classnames/bind'

interface IRoomSelectorProps {
  rooms: string[]
  selected: string
  onRoomChange: (room: string) => void
}

const cx = classNames.bind(styles)

export const RoomSelector: FC<IRoomSelectorProps> = ({ rooms, selected, onRoomChange }) => {
  return (
    <ul className={styles.list}>
      {rooms.map((room) => (
        <li
          key={room}
          className={cx(styles.room, { selected: selected === room })}
          onClick={() => onRoomChange(room)}
        >
          #{room}
        </li>
      ))}
    </ul>
  )
}
