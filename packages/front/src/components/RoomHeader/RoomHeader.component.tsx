import { FC } from 'react'
import styles from './RoomHeader.module.scss'

interface IRoomHeaderProps {
  name: string
}

export const RoomHeader: FC<IRoomHeaderProps> = ({ name }) => (
  <div className={styles.header}>
    <div className={styles.title}>#{name}</div>
  </div>
)
