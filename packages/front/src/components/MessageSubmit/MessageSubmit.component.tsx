import { FC, useState } from 'react'
import styles from './MessageSubmit.module.scss'

interface IMessageSubmitProps {
  onSubmit: (message: string) => void
}

export const MessageSubmit: FC<IMessageSubmitProps> = ({ onSubmit }) => {
  const [message, setMessage] = useState('')

  function handleSubmit() {
    setMessage('')
    onSubmit(message)
  }

  return (
    <div className={styles.form}>
      <input
        className={styles.input}
        onKeyPress={(e) => {
          if (e.key === 'Enter') {
            handleSubmit()
          }
        }}
        type="text"
        value={message}
        onChange={(e) => setMessage(e.target.value)}
        placeholder="Type a message here"
      />
      <button className={styles.button} onClick={handleSubmit}>
        Envoyer
      </button>
    </div>
  )
}
