import { FC, PropsWithChildren } from 'react'
import styles from './LeftBar.module.scss'
import { ReactComponent as BrandIcon } from './logo.svg'

export const LeftBar: FC<PropsWithChildren> = ({ children }) => (
  <div className={styles.rooms}>
    <BrandIcon />
    {children}
  </div>
)
