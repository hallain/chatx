export interface IMessage {
  id: string
  nickname: string
  timestamp: Date
  text: string
  room: string
}

export type IMessageCreation = Omit<IMessage, 'timestamp' | 'id'>
