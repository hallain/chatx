import { useState } from 'react'
import styles from './App.module.scss'
import { LeftBar } from './components/LeftBar'
import { MessagesList } from './components/MessagesList'
import { MessageSubmit } from './components/MessageSubmit'
import { NicknameForm } from './components/NicknameForm/Nicknameform.component'
import { RoomHeader } from './components/RoomHeader'
import { RoomSelector } from './components/RoomSelector'
import { useMessages } from './hooks/useMessages'

const ROOM_LIST = ['general', 'random', 'jobs']

function App() {
  const [room, setRoom] = useState('general')
  const [nickname, setNickname] = useState('')
  const [editingNickname, setEditingNickname] = useState(true)
  const { messages, connected, postMessage } = useMessages(room)

  return (
    <div className={styles.page}>
      <LeftBar>
        <div className={styles.nickname}>Nickname : {nickname}</div>
        <RoomSelector rooms={ROOM_LIST} selected={room} onRoomChange={setRoom} />
      </LeftBar>
      {connected ? (
        <>
          {editingNickname ? (
            <NicknameForm
              onClose={(name) => {
                setNickname(name)
                setEditingNickname(false)
              }}
            />
          ) : (
            <>
              <RoomHeader name={room} />
              <MessagesList messages={messages} />
              <MessageSubmit onSubmit={(text) => postMessage({ text, room, nickname })} />
            </>
          )}
        </>
      ) : (
        <h1>Connecting ...</h1>
      )}
    </div>
  )
}

export default App
