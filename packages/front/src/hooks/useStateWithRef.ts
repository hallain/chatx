import { MutableRefObject, useCallback, useRef, useState } from 'react'

// hook pour fournir une ref react en plus d'un state classique (utile par exemple pour les callbacks d'events qui peuvent utiliser la ref pour être tout le temps à jour)
export function useStateWithRef<T>(initialValue: T): [T, (value: T) => void, MutableRefObject<T>] {
  const [state, _setState] = useState<T>(initialValue)
  const stateRef = useRef(state)

  const setState = useCallback((data: T) => {
    stateRef.current = data
    _setState(data)
  }, [])

  return [state, setState, stateRef]
}
