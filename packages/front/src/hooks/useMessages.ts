import { useEffect, useRef, useState } from 'react'
import { useStateWithRef } from './useStateWithRef'
import { IMessage, IMessageCreation } from '../model/message'

const ROOT_URL = 'http://localhost:8080' // Oui, c'est sale.

export const useMessages = (roomName: string) => {
  const source = useRef<EventSource>()
  const [connected, setConnected] = useState(false)
  const [error, setError] = useState(false)
  const [messages, setMessages, messagesRef] = useStateWithRef<IMessage[]>([])

  useEffect(() => {
    source.current = new EventSource(`${ROOT_URL}/sse/${roomName}`)

    const handleMessage = (e: MessageEvent) => {
      console.log(e)
      setMessages([...messagesRef.current, ...JSON.parse(e.data)])
    }

    const handleOpen = () => {
      setConnected(true)
    }

    const handleClose = () => {
      setConnected(false)
    }

    const handleError = (e: any) => {
      setError(true)
      setConnected([EventSource.CLOSED, EventSource.CONNECTING].includes(e.readyState))
    }

    source.current.addEventListener('message', handleMessage, false)
    source.current.addEventListener('open', handleOpen, false)
    source.current.addEventListener('close', handleClose, false)
    source.current.addEventListener('error', handleError, false)

    return () => {
      if (source.current) {
        source.current.removeEventListener('message', handleMessage, false)
        source.current.removeEventListener('open', handleOpen, false)
        source.current.removeEventListener('close', handleClose, false)
        source.current.removeEventListener('error', handleError, false)
        source.current.close()
      }
      setConnected(false)
      setMessages([])
    }
  }, [roomName, source, messagesRef, setMessages])

  async function postMessage(message: IMessageCreation) {
    await fetch(`${ROOT_URL}/message`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(message),
    })
  }

  return {
    connected,
    error,
    messages,
    postMessage,
  }
}
