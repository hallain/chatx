import { format, formatRelative } from 'date-fns'
import { enUS } from 'date-fns/locale'

const formatRelativeLocale = {
  today: `HH:mm`,
}

const locale = {
  ...enUS,
  formatRelative: (token: keyof typeof formatRelative) => formatRelativeLocale[token],
}

export const humanReadableDate = (date: Date) => {
  return format(new Date(date), 'd MMMM yyyy - HH:mm')
}

export const distanceDate = (date: Date) => {
  return formatRelative(new Date(date), new Date(), { locale })
}
