# ChatX

## Realtime web chat application

![](screenshot.png)

## Fait (Niveau 2 atteint)

#### Fonctionnalités :

- L'utilisateur peut définir son nickname pour le chat au lancement de l'app
- L'utilisateur arrive sur une room par défaut (#general)
- L'utilisateur peut voir les 10 derniers messages d'une room à sa connexion
- L'utilisateur recoit en temps réel tous les messages qui ont été postés dans la room ou il est
- L'utilisateur peut changer de room (parmis une liste hard codée dans le front)

#### Reflexion UX :

- L'utilisateur ne peut pas poster de message s'il est déconnecté du serveur (l'application se reconnectera automatiquement dès que possible)
- L'ajout d'un message effectue un scroll vers le dernier message automatiquement
- Scroll "pleine page" (rend plus facile la mise à disposition éventuelle d'une version "mobile")
- L'appui sur la touche "Entrée" envoie le message (ou via le bouton Envoyer)

## Aurait aimé faire :

- Deployer l'app pour qu'elle révolutionne la facon dont on crée des liens sur le web entre les individus sensibles que nous sommes
- Un partage des interfaces TS entre le back et le front (c'est deja dans un monorepo, reste juste a build et referencer)
- Faire du code plus propre et surtout "testé" (Avec un périmètre aussi étendu et si peu de temps recommandé, c'est difficile)
- Gestion des erreurs "HTTP" côté front via un global handler et dans l'ui (L'api renvoie déjà le plus souvent les bons codes)
- Cookie de session au lieu de passer systématiquement la room et le nickname de l'utilisateur
- Compatibilité mobile - Des tas de trucs pour améliorer l'UX
- Persistance des données côté back (actuellement "in memory" dans un repository basé sur du "let" :poop:, mais facile a faire évoluer pour le coup, avec du "lowdb" par exemple)
- Intégrer un routeur côté front pour avoir une approche url-first (éviterait le souc dans le app.tsx avec des ternaires de ternaires de ternaires de ternaires ... mais c'est comme ca.)
- Mettre en place un storybook

## instruction de lancement de l'app

Ce projet utilise [Yarn](https://classic.yarnpkg.com/en/docs/install) (v1) en tant que gestionnaire de dépendances.

Pour installer les dépendances, lancer la commande :

```
yarn --frozen-lockfile
```

Pour démarrer le projet, lancer la commande :

```
yarn start
```

Normalement (ou [paranormalement](https://youtu.be/SHJ1HL4eMLQ?t=705)), votre navigateur par défaut devrait s'ouvrir sur l'url `http://localhost:3000` et afficher l'application.
